import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HompePage extends StatelessWidget {
  const HompePage({super.key});

  void logout() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Museum Jabar",
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Selamat Datang di Museum Jabar"),
            SizedBox(
              height: 15.0,
            ),
            ElevatedButton.icon(
              style: ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.deepPurple),
              ),
              onPressed: logout,
              icon: Icon(Icons.logout_outlined),
              label: Text("LogOut"),
            )
          ],
        ),
      ),
    );
  }
}
